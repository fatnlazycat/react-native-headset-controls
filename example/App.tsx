import { useEffect } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import * as RNExpoRemoteControls from 'rn-expo-remote-controls';

export default function App() {
  useEffect(() => {
    RNExpoRemoteControls.startListening({
      onPlay: () => console.log('on play'),
    });
    return () => RNExpoRemoteControls.stopListening();
  }, []);

  return (
    <View style={styles.container}>
      <Text>{JSON.stringify(RNExpoRemoteControls)}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
