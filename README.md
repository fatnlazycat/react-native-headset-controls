# rn-expo-remote-controls

A module for listening the remote media controls (currently iOS only). 

Under the hood uses `AVAudioPlayer` which plays a short (250ms) audio file when initializing the module to make the app eligible to receive remote media control events from the OS. 

# API documentation

```
export const startListening = (
  listeners: Partial<{
    onPlay: () => void;
    onPause: () => void;
    onNextTrack: () => void;
    onPrevTrack: () => void;
  }>,
): void

export const stopListening = (): void
```

#### Usage

```
import * as RemoteControls from '@fatnlazycat/rn-expo-remote-controls';
```

```
  useEffect(() => {
    RemoteControls.startListening({
      onPlay: // play command logic
      onPause: // pause command logic
      onNextTrack: // next track command logic
      onPrevTrack: // prevoius track command logic
    });
    return RemoteControls.stopListening;
  }, [startAutoPlayback, stopAutoPlayback]);
  ```

# Installation in managed Expo projects
```
npm install @fatnlazycat/rn-expo-remote-controls
```

## Installation in bare React Native projects

For bare React Native projects, you must ensure that you have [installed and configured the `expo` package](https://docs.expo.dev/bare/installing-expo-modules/) before starting.


### Configure for iOS

Run `npx pod-install` after installing the npm package.


### Configure for Android

n/a
