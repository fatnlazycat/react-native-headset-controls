//
//  RemoteCommandHandler.swift
//  RNExpoRemoteControls
//
//  Created by Dmitriy Ko on 05.02.2024.
//

import ExpoModulesCore
import MediaPlayer

class RemoteCommandHandler {
    let listenerFactory: (_ event: EventType) -> ((MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus)
    let commandCenter = MPRemoteCommandCenter.shared()
    
    init(_ emitter: BaseModule) {
        listenerFactory = { [unowned emitter] eventType in
            return { event in
                print("event \(event)")
                if event.command.isEnabled {
                    print("Toggle play/pause state or perform any action")
                    emitter.sendEvent(eventType.rawValue)
                    return .success
                }
                return .commandFailed
            }
        }
        
        commandCenter.playCommand.addTarget(handler: listenerFactory(.onPlay))
        commandCenter.pauseCommand.addTarget(handler: listenerFactory(.onPause))
        commandCenter.nextTrackCommand.addTarget(handler: listenerFactory(.onNext))
        commandCenter.previousTrackCommand.addTarget(handler: listenerFactory(.onPrevious))
//        Volume change command
//        commandCenter.changePlaybackPositionCommand.addTarget(handler: listenerFactory(.onChangePosition))

        playInitialAudio()
    }
    
    deinit {
        commandCenter.playCommand.removeTarget(nil)
        commandCenter.pauseCommand.removeTarget(nil)
        commandCenter.nextTrackCommand.removeTarget(nil)
        commandCenter.previousTrackCommand.removeTarget(nil)
    }
    
    func playInitialAudio() {
        guard let path = Bundle.main.path(forResource: "250-milliseconds-of-silence.mp3", ofType: nil) else {
            print("no music file")
            return
        }
        
        let url = URL(fileURLWithPath: path)
        
        do {
            if let player = try? AVAudioPlayer(contentsOf: url) {
                try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
                try AVAudioSession.sharedInstance().setActive(true)
                player.prepareToPlay()
                print("player \(player)")
                player.play()
            }
        } catch let error {
            print(error.localizedDescription)
        }
    }
}


