import ExpoModulesCore
import MediaPlayer

public class RNExpoRemoteControlsModule: Module {
  // Each module class must implement the definition function. The definition consists of components
  // that describes the module's functionality and behavior.
  // See https://docs.expo.dev/modules/module-api for more details about available components.
    
  var remoteCommandHandler: RemoteCommandHandler?

  public func definition() -> ModuleDefinition {
    // Sets the name of the module that JavaScript code will use to refer to the module. Takes a string as an argument.
    // Can be inferred from module's class name, but it's recommended to set it explicitly for clarity.
    // The module will be accessible from `requireNativeModule('RNExpoRemoteControls')` in JavaScript.
    Name("RNExpoRemoteControls")

    // Defines event names that the module can send to JavaScript.
    Events(EventType.onPlay.rawValue,
         EventType.onPause.rawValue,
         EventType.onNext.rawValue,
         EventType.onPrevious.rawValue)
      
    Function("startListening") {
        print("startListening!")
        self.remoteCommandHandler = RemoteCommandHandler(self)
    }
      
    Function("stopListening") {
        print("stopListening")
        self.remoteCommandHandler = nil
    }
  }
}

enum EventType: String {
    case onPlay = "onPlay"
    case onPause = "onPause"
    case onNext = "onNext"
    case onPrevious = "onPrevious"
}

