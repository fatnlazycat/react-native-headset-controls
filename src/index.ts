import { NativeModulesProxy, EventEmitter, Subscription } from 'expo-modules-core';

// Import the native module. On web, it will be resolved to RNExpoRemoteControls.web.ts
// and on native platforms to RNExpoRemoteControls.ts
import { ChangeEventPayload, RemoteControlsEvent } from './RNExpoRemoteControls.types';
import RNExpoRemoteControlsModule from './RNExpoRemoteControlsModule';

const emitter = new EventEmitter(RNExpoRemoteControlsModule ?? NativeModulesProxy.RNExpoRemoteControls);

export function addChangeListener(eventType: RemoteControlsEvent, listener: (event: ChangeEventPayload) => void): Subscription {
  return emitter.addListener<ChangeEventPayload>(eventType, listener);
}

export const startListening = (
  listeners: Partial<{
    onPlay: () => void;
    onPause: () => void;
    onNextTrack: () => void;
    onPrevTrack: () => void;
  }>,
): void => {
  const { onPlay, onPause, onNextTrack, onPrevTrack } = listeners;
  onPlay && emitter.addListener(RemoteControlsEvent.onPlay, onPlay);
  onPause && emitter.addListener(RemoteControlsEvent.onPause, onPause);
  onNextTrack && emitter.addListener(RemoteControlsEvent.onNext, onNextTrack);
  onPrevTrack && emitter.addListener(RemoteControlsEvent.onPrevious, onPrevTrack);
  RNExpoRemoteControlsModule.startListening();
};

export const stopListening = (): void => {
  emitter.removeAllListeners(RemoteControlsEvent.onPlay);
  emitter.removeAllListeners(RemoteControlsEvent.onPause);
  emitter.removeAllListeners(RemoteControlsEvent.onNext);
  emitter.removeAllListeners(RemoteControlsEvent.onPrevious);
  RNExpoRemoteControlsModule.stopListening();
};

export { ChangeEventPayload };
