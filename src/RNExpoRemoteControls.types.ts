export type ChangeEventPayload = {
  value: string;
};

export enum RemoteControlsEvent {
  onPlay = 'onPlay',
  onPause = 'onPause',
  onNext = 'onNext',
  onPrevious = 'onPrevious',
}
